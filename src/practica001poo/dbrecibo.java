/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica001poo;

import java.sql.*;

/**
 *
 * @author Iker Martinez
 */
public class dbrecibo {
    private String MYSQLDRIVER = "com.mysql.cj.jdbc.Driver";
    private String MYSQLDB = "jdbc:mysql://3.132.136.208:3306/ikuer?user=ikuer&password=lviker23";
    private Connection conexion;
    private String strConsulta;
    private ResultSet registro;

    private Recibo re = new Recibo();

    public dbrecibo() {
        try {
            Class.forName(MYSQLDRIVER);
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR EN " + e.getMessage());
            System.exit(-1);
        }
    }

    public void conectar() {
        try {
            conexion = DriverManager.getConnection(MYSQLDB);
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("NO SE LOGRO LA CONEXION " + e.getMessage());
        }
    }

    public void desconectar() {
        try {
            this.conexion.close();
        } catch (SQLException e) {
            
            System.out.println("NO SE LOGRO DESCONECTAR " + e.getMessage());
        }
    }

    public void insertar(Recibo re) {
        this.conectar();
        try {
            this.strConsulta = "INSERT INTO recibo(numrecibo, fecha, nombre, domicilio, tipo, costo, consumidos, status) VALUES (?,CURDATE(),?,?,?,?,?,?);";

            PreparedStatement pst = this.conexion.prepareStatement(this.strConsulta);
            pst.setInt(1, re.getNumRecibo());
            //pst.setString(2, re.getFecha());
            pst.setString(2, re.getNom());
            pst.setString(3, re.getDom());
            pst.setInt(4, re.getTipo());
            pst.setFloat(5, re.getCostKw());
            pst.setFloat(6, re.getKwConsu());
            pst.setInt(7, re.getStatus());

            pst.executeUpdate();
        } catch (SQLException e) {
            
            System.err.println("Error al insertar " + e.getMessage());
        }
        this.desconectar();
    }

    public void actualizar(Recibo re) {
        strConsulta = "UPDATE recibo SET nombre = ?, domicilio = ?, fecha = ?, tipo = ?, costo = ?, consumidos = ? WHERE numrecibo = ? AND status = 0;";
        this.conectar();
        try {
            PreparedStatement pst = this.conexion.prepareStatement(this.strConsulta);
            pst.setString(1, re.getNom());
            pst.setString(2, re.getDom());
            pst.setString(3, re.getFecha());
            pst.setInt(4, re.getTipo());
            pst.setFloat(5, re.getCostKw());
            pst.setFloat(6, re.getKwConsu());
            pst.setInt(7, re.getNumRecibo());

            pst.executeUpdate();
        } catch (SQLException e) {
            
            System.err.println("Surgio un error al actualizar" + e.getMessage());
        }
    }

    public void habilitar(Recibo re) {
        this.strConsulta = "UPDATE recibo SET status = 0 WHERE numrecibo = ?";
        this.conectar();
        try {
            System.err.println("se conecto");
            PreparedStatement pst = this.conexion.prepareStatement(this.strConsulta);
            pst.setInt(1, re.getNumRecibo());

            pst.executeUpdate();
            desconectar();
        } catch (SQLException e) {
            
            System.err.println("Surgio un error al habilitar " + e.getMessage());
        }
    }

    public void deshabilitar(Recibo re) {
        this.strConsulta = "UPDATE recibo SET status = 1 WHERE numrecibo = ?";
        this.conectar();
        try {
            System.err.println("se conecto");
            PreparedStatement pst = this.conexion.prepareStatement(this.strConsulta);
            pst.setInt(1, re.getNumRecibo());

            pst.executeUpdate();
            desconectar();
        } catch (SQLException e) {
            
            System.out.println("Surgio un error al habilitar " + e.getMessage());
        }
    }

    public boolean isExiste(int numRecibo, int status) {
        boolean exito = false;
        this.conectar();
        this.strConsulta = "SELECT * FROM recibo WHERE numrecibo = ? AND status = ?;";
        try {
            PreparedStatement pst = this.conexion.prepareStatement(this.strConsulta);
            pst.setInt(1, numRecibo);
            pst.setInt(2, status);

            this.registro = pst.executeQuery();
            if (this.registro.next()) exito = true;
        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("Surgio un error al verificar si existia:" + e.getMessage());
        }
        this.desconectar();
        return exito;
    }

    public Recibo buscar(int numRecibo) {
        conectar();
        try {
            strConsulta = "SELECT * FROM recibo WHERE numrecibo = ? AND status = 0;";
            PreparedStatement pst = this.conexion.prepareStatement(this.strConsulta);

            pst.setInt(1, numRecibo);
            this.registro = pst.executeQuery();
            if (this.registro.next()) {
                re.setId(this.registro.getInt("id"));
                re.setNumRecibo(registro.getInt("numrecibo"));
                re.setNom(registro.getString("nombre"));
                re.setDom(this.registro.getString("domicilio"));
                re.setTipo(this.registro.getInt("tipo"));
                re.setCostKw(this.registro.getFloat("costo"));
                re.setKwConsu(this.registro.getFloat("consumidos"));
                re.setFecha(this.registro.getString("fecha"));
            } else re.setId(0);
        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("Surgio un problema al habilitar:" + e.getMessage());
        }
        this.desconectar();
        return re;
    }
    
}
